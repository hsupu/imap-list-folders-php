<?php
/**
 * @author xp
 */
return [
    'server' => 'imap.example.com:993',
    'user' => '',
    'pass' => '',
    'flags' => ['imap', 'readonly', 'ssl', 'novalidate-cert'],
    'encoding' => 'UTF-8',
];
