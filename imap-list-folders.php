<?php
/**
 * @author xp
 */
require 'vendor/autoload.php';

$config = require 'config.php';

$flags = implode('/', $config['flags']);
$mailbox = new PhpImap\Mailbox(
    '{' . $config['server'] . '/' . $flags . '}',
    $config['user'],
    $config['pass'],
    __DIR__,
    $config['encoding']
);

try {
    $folders = $mailbox->getMailboxes('*');
    $dirs = [];
    foreach ($folders as $folder) {
        $dirs[] = $folder['shortpath'];
    }
    echo implode("\n", $dirs) . "\n";
} catch (\Exception $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}
