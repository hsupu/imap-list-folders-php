# README

A simple script to list IMAP folders for PHP.

## requirement

```bash
apt install -y php-fileinfo php-iconv php-imap php-mbstring
```

see `composer.json` :: `dependencies`.

## usage

```bash
composer install
cp config-example.php config.php
vim config.php
```

```
> ./run.sh
INBOX
Sent Messages
Drafts
Deleted Messages
Junk
```
